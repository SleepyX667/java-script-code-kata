console.log("Test0-A");
var set = new Set();
var a = new Dwellers(1, 1, 1);
var b = new Dwellers(1, 1, 1);
set.add(a);
set.add(b);
console.log(set);

console.log("Test0-B");
var dwellers1 = new Dwellers(2, 2, 2);
var dwellers2 = new Dwellers(2, 2, 2);
var bool = dwellers1.equals(dwellers2);
console.log(bool);

console.log("Test1");
var dwellers1 = new Dwellers(1, 1, 1);
console.log(dwellers1);
console.log("wolfEatsGoat");
var result1 = dwellers1.wolfEatsGoat();
console.log(result1);

console.log("Test2");
var dwellers2 = new Dwellers(1, 1, 1);
console.log(dwellers2);
console.log("lionEatsGoat");
var result2 = dwellers2.lionEatsGoat();
console.log(result2);

console.log("Test3");
var dwellers3 = new Dwellers(1, 1, 1);
console.log(dwellers3);
console.log("lionEatsWolf");
var result3 = dwellers3.lionEatsWolf();
console.log(result3);

console.log("Test4");
var dwellers4 = new Dwellers(0, 0, 0);
console.log(dwellers4);
console.log("lionEatsWolf");
var result4 = dwellers4.lionEatsWolf();
console.log(result4);

console.log("Test5");
var magicForest = new MagicForest(1, 1, 1);
console.log(magicForest);
console.log("nextIteration");
magicForest.nextIteration();
console.log(magicForest);

console.log("Test6");
var magicForest2 = new MagicForest(1, 1, 1);
console.log(magicForest2);
magicForest2.magicLife();
console.log(magicForest2);

console.log("Test7");
var magicForest3 = new MagicForest(0, 0, 5);
console.log(magicForest3);
magicForest3.magicLife();
console.log(magicForest3);


console.log("Test8");
var magicForest4 = new MagicForest(17, 55, 6);
console.log(magicForest4);
magicForest4.magicLife();
console.log(magicForest4);
