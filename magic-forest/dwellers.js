class Dwellers {
  constructor(goats, wolves, lions) {
      this.goats = goats;
      this.wolves = wolves;
      this.lions = lions;
  }

  wolfEatsGoat() {
    if ((this.goats > 0) && (this.wolves > 0)) {
      return new Dwellers(this.goats - 1,  this.wolves - 1, this.lions + 1);
    } else {
      return null;
    }
  }

  lionEatsGoat() {
    if ((this.goats > 0) && (this.lions > 0)) {
      return new Dwellers(this.goats - 1,  this.wolves + 1, this.lions - 1);
    } else {
      return null;
    }
  }

  lionEatsWolf() {
    if ((this.wolves > 0) && (this.lions > 0)) {
      return new Dwellers(this.goats + 1, this.wolves - 1, this.lions - 1);
    } else {
      return null;
    }
  }

  equals(other) {
    if (other == null) {
      return false;
    }
    return (this.goats == other.goats) && (this.wolves == other.wolves) && (this.lions == other.lions);
  }
}
