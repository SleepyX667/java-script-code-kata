class MagicForest {
  constructor(goats, wolves, lions) {
    var dwellers = new Dwellers(goats, wolves, lions);
    this.current = new Set();
    this.current.add(dwellers);
    this.finish = false;
    this.solution = null;
  }

  adding(list, newDweller) {
    if (newDweller == null) {
      return list;
    }
    var insert = true;
    list.forEach(function (containedDweller) {
      if (newDweller.equals(containedDweller)) {
        insert = false;
      }
    }, this);
    if (insert) {
      list.push(newDweller);
    }
    return list;
  }

  nextIteration() {
    var self = this;
    var temp = [];
//    console.log(this.current.size);
    if (this.current.size != 0) {
      this.current.forEach(function (dwellers) {
        var result1 = dwellers.wolfEatsGoat();
        var result2 = dwellers.lionEatsGoat();
        var result3 = dwellers.lionEatsWolf();

        if (result1 == null && result2 == null && result3 == null) {
          console.log("got a result");
          this.solution = dwellers;
          this.finish = true;
        }
        temp = self.adding(temp, result1);
        temp = self.adding(temp, result2);
        temp = self.adding(temp, result3);

      }, this);
      this.current = temp;
    } else {
      this.finish = true;
    }
  }

  magicLife() {
     while (!this.finish) {
//       console.log(this.current);
       this.nextIteration();
     }
     console.log(this.solution);
  }

  foundSomething() {
    console.log("got a result");
    console.log(dwellers);
    this.solution = dwellers;
    this.finish = true;
  }
}
